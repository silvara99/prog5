import logging
import rds_config
import pymysql
import sys

rds_host = "demo.coiov5hsujqo.us-east-1.rds.amazonaws.com"
name = rds_config.db_username
password = rds_config.db_password
db_name = rds_config.db_name

logger = logging.getLogger()
logger.setLevel(logging.INFO)

try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, connect_timeout=5)
except:
    logger.error("ERROR: no se pudo conectar a MySQL.")
    sys.exit()

logger.info("SUCCESS: Se ha establecido la conexion a MySQL satisfactoriamente")
def handler(event, context):
    with conn.cursor() as cur:
        total_numeros = 0
        cuenta_numeros = 0
        try:
            if event['currentIntent']['slots']:
                cur.execute("TRUNCATE TABLE flotantes")
                logger.info("Slots:")
                for num in event['currentIntent']['slots']:
                    if event['currentIntent']['slots'][num] is not None:
                        logger.info(event['currentIntent']['slots'][num])
                        sql = "INSERT INTO `flotantes` (`float`) VALUES (%s)"
                        cur.execute(sql, (event['currentIntent']['slots'][num]))
        except:
            pass
        cur.execute("select * from flotantes")
        logger.info("Filas en DB:")
        for row in cur:
            if not row[1] == 0:
                total_numeros += row[1]
                cuenta_numeros += 1
                logger.info(row)
    conn.commit()
    prom = total_numeros / cuenta_numeros

    response = {
        "dialogAction": {
            "type": "Close",
            "fulfillmentState": "Fulfilled",
            "message": {
              "contentType": "SSML",
              "content": "El promedio es {promedio}".format(promedio=prom)
            },
        }
    }
    print('result = ' + str(response))
    conn.close()
    return response