import logging
import rds_config
import pymysql
import sys

rds_host = "demo.coiov5hsujqo.us-east-1.rds.amazonaws.com"
name = rds_config.db_username
password = rds_config.db_password
db_name = rds_config.db_name

logger = logging.getLogger()
logger.setLevel(logging.INFO)

try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, connect_timeout=5)
except:
    logger.error("ERROR: no se pudo conectar a MySQL.")
    sys.exit()

logger.info("SUCCESS: Se ha establecido la conexion a MySQL satisfactoriamente")
def handler(event, context):
    cuenta_personas = 0

    with conn.cursor() as cur:
        cur.execute("select * from personas")
        for row in cur:
            cuenta_personas += 1
            logger.info(row)
    conn.commit()
    conn.close()
    return "Hay %d personas en el equipo." %(cuenta_personas)