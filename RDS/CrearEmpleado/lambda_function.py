import json
import datetime
import sys
import pymysql
import rds_config

serv = "demo.coiov5hsujqo.us-east-1.rds.amazonaws.com"
name = rds_config.db_username
passw = rds_config.db_password
dbnm = rds_config.db_name
tbnm = rds_config.db_table

crear_db = "CREATE DATABASE %s;" % (dbnm)

crear_tabla = ("""
CREATE TABLE `%s` (
  `cedula` varchar(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `departamento` varchar(100) NOT NULL,
  `puesto` varchar(100) NOT NULL,
  `salario` float NOT NULL,
  PRIMARY KEY (`cedula`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='	';
""" % (tbnm))

try:
    conn = pymysql.connect(serv, name, passwd=passw, db=dbnm)
    with conn.cursor() as cur:
        cur.execute(crear_tabla)
        cur.commit()
except:
    try:
        conn = pymysql.connect(serv, name, passwd=passw)
        with conn.cursor() as cur:
            cur.execute(crear_db)
            cur.commit()
    except:
        pass
finally:
        try:
            conn = pymysql.connect(serv, name, passwd=passw, db=dbnm)
            with conn.cursor() as cur:
                cur.execute(crear_tabla)
                cur.commit()
        except:
            conn = pymysql.connect(serv, name, passwd=passw, db=dbnm)

def lambda_handler(event, context):
    
    cedula = event['currentIntent']['slots']['cedula']
    nombre = event['currentIntent']['slots']['nombre']
    apellido = event['currentIntent']['slots']['apellido']
    fecha_nacimiento = event['currentIntent']['slots']['fecha_nacimiento']
    departamento = event['currentIntent']['slots']['departamento']
    puesto = event['currentIntent']['slots']['puesto']
    salario = float(event['currentIntent']['slots']['salario'])
    
    with conn.cursor() as cur:
        try:
            cur.execute('INSERT INTO empleado VALUES("%s", "%s", "%s", "%s", "%s", "%s", %f)' % (cedula, nombre, apellido, fecha_nacimiento, departamento, puesto, salario))
            conn.commit()
            response = {
                "dialogAction": {
                    "type": "Close",
                    "fulfillmentState": "Fulfilled",
                    "message": {
                        "contentType": "PlainText",
                        "content": "Se ha creado el colaborador %s %s con exito" % (nombre, apellido)
                    }
                 }
            }
        except:
            pass
            try:
                cur.execute(crear_tabla)
                cur.commit()
                cur.execute('INSERT INTO empleado VALUES("%s", "%s", "%s", "%s", "%s", "%s", %f)' % (cedula, nombre, apellido, fecha_nacimiento, departamento, puesto, salario))
                conn.commit()
                response = {
                    "dialogAction": {
                        "type": "Close",
                        "fulfillmentState": "Fulfilled",
                        "message": {
                            "contentType": "PlainText",
                            "content": "Se ha creado el colaborador %s %s con exito" % (nombre, apellido)
                        }
                     }
                }
            except:
                cur.execute('SELECT COUNT(*) FROM `empleado` WHERE `cedula`=%s', (cedula))
                row_count = cur.fetchone()[0]
                print (row_count)
                if row_count == 1:
                    response = {
                        "dialogAction": {
                            "type": "Close",
                            "fulfillmentState": "Fulfilled",
                            "message": {
                                "contentType": "PlainText",
                                "content": "El colaborador %s %s ya existe en la base de datos" % (nombre, apellido)
                            }
                         }
                    }
                elif row_count == 0:
                    response = {
                        "dialogAction": {
                            "type": "Close",
                            "fulfillmentState": "Fulfilled",
                            "message": {
                                "contentType": "PlainText",
                                "content": "No se encontro en la base de datos"
                            }
                         }
                    }
                else:
                    response = {
                        "dialogAction": {
                            "type": "Close",
                            "fulfillmentState": "Fulfilled",
                            "message": {
                                "contentType": "PlainText",
                                "content": "ERROR"
                            }
                         }
                    }

    try:
        conn.close()
    except:
        pass
    return response