import boto3

rekognition = boto3.client("rekognition")

def lambda_handler(event, context):
    #print (event)
    
    bucket = event["Records"][0]["s3"]["bucket"]["name"]
    photo = event["Records"][0]["s3"]["object"]["key"]
    
    response = rekognition.detect_faces(Image={"S3Object":{"Bucket":bucket,"Name":photo}},Attributes=["ALL"])
    
    for faceDetail in response["FaceDetails"]:
        if faceDetail["Smile"]["Value"] == True:
            print("La persona esta sonriendo")
        elif faceDetail["Smile"]["Value"] == False:
            print("La persona no esta sonriendo")    
        #print(faceDetail)
    
    personas= ("Hay "+ str(len(response["FaceDetails"])) + " personas en esta foto")
    print (personas)
    return personas